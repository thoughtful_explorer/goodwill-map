# Goodwill Map
Extracts Goodwill store locations from a geolocation API within the official [Goodwill](https://www.goodwill.org) website and places them into KML format.

## Dependencies
* Python 3.x
    * Requests module for session functionality
    * Simplekml for easily building KML files
    * JSON module for JSON-based geodata
    * CSV module to read a CSV file of geographic coordinates
* Also of course depends on the [Goodwill GeoLocAPI](https://www.goodwill.org/GetLocAPI.php).
