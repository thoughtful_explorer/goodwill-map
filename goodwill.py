#!/usr/bin/python3

import requests
import simplekml
import json
from csv import reader

#Set filename/path for KML file output
kmlfile = "goodwill.kml"
#Set KML schema name
kmlschemaname = "goodwill"
#Set page URL
pageURL = "https://www.goodwill.org/GetLocAPI.php"
#Set locations CSV
postalpointsCSV = "postalpoints.csv"

#Tests a given URL to see if the server responds. Returns True if so, raises an exception if not
def testurl(url):
	try:
		#Get Url
		get = requests.get(url)
		# if the request succeeds
		if get.status_code == 200:
			return True
		else:
			return False
	#Exception
	except requests.exceptions.RequestException as e:
        # print URL with errors
		return False

#Returns a list of lat,lng point coordinates from a given CSV file
def getpostalpoints(postalpointsfile=postalpointsCSV):
    #Initialize a points list to append to
    points = []
    #Open the CSV file as read-only
    with open(postalpointsfile, "r") as csvfile:
        #Create a reader object to process the lines from the CSV file
        pointsfile = reader(csvfile)
        #Skip the column headers
        header = next(pointsfile)
        #Iterate through each point (row) in the file
        for point in pointsfile:
            #Append each to the points list
            points.append(point)
    #Return the points list
    return points

#Example JSON from API calls:
'''
{
        "LocationId": "2be6cd81-458a-e111-8dde-005056934050",
        "LocationName": "Port Angeles Retail Store",
        "LocationLatitude1": 48.11389,
        "LocationLongitude1": -123.43562,
        "LocationStreetAddress1": "603 South Lincoln Street",
        "LocationCity1": "Port Angeles",
        "LocationState1": "WA",
        "LocationPostal1": "98362-6111",
        "LocationPhoneOffice": "(360) 452-2440",
        "calcd_ServicesOffered": "Donation Center,Retail Store",
        "ci_servR": 17,
        "ci_servO": 0,
        "ci_servD": 1,
        "ci_servS": 0,
        "ci_servCS": 0,
        "ci_servSR": 0,
        "LocationParentWebsite": "http://www.goodwillwa.org",
        "Name_Parent": "Goodwill of the Olympics and Rainier Region",
        "Phone_Parent": "(253) 573-6500",
        "LocationParentURLFacebook": null,
        "LocationParentURLTwitter": null,
        "LocationParentURLOnlineGivingURL": "https://goodwillwa.org/get-involved/give/",
        "LocationParentStreetAddress": "714 South 27th Street",
        "LocationParentCity": "Tacoma",
        "LocationParentState": "WA",
        "LocationParentPostal": "98409-8193",
        "LocationParentCountry": "United States",
        "calcd_ParentCity_wAlias": "Tacoma",
        "ServiceName": null,
        "MonOpeningTime": null,
        "MonClosingTime": null,
        "TueOpeningTime": null,
        "TueClosingTime": null,
        "WedOpeningTime": null,
        "WedClosingTime": null,
        "ThuOpeningTime": null,
        "ThuClosingTime": null,
        "FriOpeningTime": null,
        "FriClosingTime": null,
        "SatOpeningTime": null,
        "SatClosingTime": null,
        "SunOpeningTime": null,
        "SunClosingTime": null,
        "Dist": 49.83,
        "LocType": 2
    },
'''

#Calls the API for all locations that match given coordinates from getpostalpoints()...
#...eliminates duplicate store locations due to inevitable geographical query overlap, and returns a unique list of locations
def getlocations():
    #Initialize a list of locations
    locations = []
    #Iterate through the points from the CSV file
    for point in getpostalpoints():
        #Form a request URL based on the coordinates of each point from the CSV file
        requestURL = pageURL+"?lat="+point[0]+"&lng="+point[1]+"&radius=20&cats=3%2C2"
        #Get the API response
        response = requests.get(requestURL)
        #Check for empty results (which this API returns as literal empty square brackets)...
        if response.text == "[]":
            print("No results.")
        else:
            #...otherwise, loop through all the stores in the results of this point query...
            for store in response.json():
                #...and append each to the locations list
                locations.append(store)
    #Ensure each location is unique by using the very convenient uuid-style LocationId field and searching the existing items in the locations list for the given LocationId
    uniquelocations = { each["LocationId"] : each for each in locations }.values()
    #Return the unique (de-duplicated) locations
    return uniquelocations

#Saves a KML file of a given list of stores
def createkml(stores):
    #Initialize kml object
    kml = simplekml.Kml()
    #Iterate through list for each store
    for store in stores:
        #Get the name of the store
        storename = store["LocationName"]
        #Get coordinates of the store
        lat = store["LocationLatitude1"]
        lng = store["LocationLongitude1"]
        #Create a full store address from the more granular address attributes
        storeaddress = str(store["LocationStreetAddress1"])+" "+str(store["LocationCity1"])+" "+str(store["LocationState1"])+" "+str(store["LocationPostal1"])
        #First, create the point name and description (using the address as the description) in the kml
        point = kml.newpoint(name=storename,description=storeaddress)
        #Finally, add coordinates to the feature
        point.coords=[(lng,lat)]
    #Save the final KML file
    kml.save(kmlfile)

#Bring it all together
createkml(getlocations())
